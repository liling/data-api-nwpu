<?php

/**
 * 获取一卡通记录
 * http://localhost/datasource/nwpu/schoolcard.php?badge_number=2012301166&begin_date=2016-05-06&end_date=2016-05-06&limit=10&offset=0
 */
$data = $_GET;
ini_set('display_errors', 0);
error_reporting(-1);

require_once('../../vendor/autoload.php');
require_once('lib.php');
include_once('weekend.php');
include_once('authconfig.php');
include_once('datasource.php');

use alsvanzelf\jsonapi;

//$api_key = $data['api_key'];
$badge_number = $data['badge_number'];
$begin_date = $data['begin_date'];
$end_date = $data['end_date'];
$limit = $data['limit'];
$offset = $data['offset'];
$badge_number;

$jsonerrors = new jsonapi\errors();
/*if (!checkIP()) {
    $error = new jsonapi\error(1);
    $error->set_friendly_detail("拒绝该IP访问");
    $error->set_http_status('403');
    $error->set_friendly_message('拒绝该IP访问');
    $jsonerrors = new jsonapi\errors($error);
    $jsonerrors->send_response();
}*/
/*if ($api_key != MYPHPAPIKEY) {
    $error = new jsonapi\error(1);
    $error->set_friendly_detail("无授权码，拒绝访问");
    $error->set_http_status('403');
    $error->set_friendly_message('无授权码，拒绝访问');
    $jsonerrors = new jsonapi\errors($error);
    $jsonerrors->send_response();
}
*/
if (empty($badge_number) || empty($begin_date) || empty($end_date) || !isset($data['limit']) || !isset($data['offset'])) {
    $error = new jsonapi\error(1);
    $error->set_friendly_detail("参数均不允许为空");
    $error->set_http_status('400');
    $error->set_friendly_message('参数错误');
    $jsonerrors = new jsonapi\errors($error);
    $jsonerrors->send_response();
}

$begin = $begin_date.' 00:00:00';
$end = $end_date.' 23:59:59';
$max = $limit * ($offset+1);
$min = $offset * $limit;

#$sql = "SELECT trade_location, trade_fee,trade_time, trade_type FROM (SELECT A.*, ROWNUM RN FROM (SELECT Y.SHMC as trade_location,TO_CHAR(Y.JYE,'fm99990.00') as trade_fee,Y.FSSJ as trade_time,Y.CZLX as trade_type FROM idc_u_cw.ykt_weblist Y WHERE Y.XGH=:xgh AND Y.FSSJ<=TO_DATE:end AND Y.FSSJ>=:begin)) A WHERE ROWNUM<=:max) WHERE RN>:min";
$sql = "SELECT Y.SHMC as trade_location,TO_CHAR(Y.JYE,'fm99990.00') as trade_fee,Y.FSSJ as trade_time,Y.CZLX as trade_type FROM idc_u_cw.ykt_weblist Y WHERE Y.XGH='$badge_number' and y.fssj>='$begin' and y.fssj<='$end' order by y.fssj desc";

$conn = oci_connect(DATABASEUSERNAME,DATABASEPASSWORD,DATABASEHOSTNAME,'UTF8');
if(!$conn){
    $error = new jsonapi\error(1);
    $error->set_friendly_detail("数据库连接失败");
    $error->set_http_status('500');
    $error->set_friendly_message('数据库连接失败');
    $jsonerrors = new jsonapi\errors($error);
    $jsonerrors->send_response();
}

// 读取内容
$stmt = oci_parse($conn, $sql);
//oci_bind_by_name($stmt, ':xgh', $badge_number);
//oci_bind_by_name($stmt, ':begin', $begin);
//oci_bind_by_name($stmt, ':end', $end);
//oci_bind_by_name($stmt, ':max', $max);
//oci_bind_by_name($stmt, ':min', $min);
oci_execute($stmt);
oci_fetch_all($stmt, $records, $offset, $limit, OCI_FETCHSTATEMENT_BY_ROW);
oci_free_statement($stmt);
oci_close($conn);

$json_data_attibutes_trade_list = array();
foreach ($records as $r) {
    $json_data_attibutes_trade_list[] = array_change_key_case($r, CASE_LOWER);
}

$authors = array('iwx team');
$copyRight = 'Copyright 2016 IWX';
$json_meta = array('authors'=>$authors,'copyright'=>$copyRight);
$json_data_attibutes = array(
    'student_badgenumber'=>$badge_number,
    'trade_list'=>array_values($json_data_attibutes_trade_list)
    );

$jsonapi = new jsonapi\resource($type="schoolcard",null);
$jsonapi->fill_meta($json_meta);
$jsonapi->fill_data($json_data_attibutes);
$jsonapi->send_response();
