<?php
function checkIP()
{
    $ip = get_real_ip();
    $ips = explode (",", MYADDRESS);
    if (in_array($ip, $ips)) {
        return true;
    } else {
        return false;
    }
}
/*
function getheaders()
{
    $headers = array(); 
    foreach ($_SERVER as $key => $value) { 
        if ('HTTP_' == substr($key, 0, 5)) { 
            $headers[str_replace('_', '-', substr($key, 5))] = $value; 
        } 
    }
    if (isset($_SERVER['PHP_AUTH_DIGEST'])) { 
        $headers['AUTHORIZATION'] = $_SERVER['PHP_AUTH_DIGEST']; 
    } elseif (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) { 
        $headers['AUTHORIZATION'] = base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW']); 
    } 
    if (isset($_SERVER['CONTENT_LENGTH'])) { 
        $headers['CONTENT-LENGTH'] = $_SERVER['CONTENT_LENGTH']; 
    } 
    if (isset($_SERVER['CONTENT_TYPE'])) { 
        $headers['CONTENT-TYPE'] = $_SERVER['CONTENT_TYPE']; 
    }
    if (isset($_SERVER["REMOTE_ADDR"])){
        $headers['REMOTE_ADDR'] = get_real_ip();
    }
    return $headers;
}
*/
function get_real_ip(){ 
    $ip=false;  
    if(!empty($_SERVER["HTTP_CLIENT_IP"])){  
        $ip = $_SERVER["HTTP_CLIENT_IP"];  
    }  
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
        $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip) { 
            array_unshift($ips, $ip); 
            $ip = FALSE; 
        }  
        for ($i = 0; $i < count($ips); $i++) {  
            if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])) {
                $ip = $ips[$i];
                break; 
            }
        }
    } 
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);  
}
