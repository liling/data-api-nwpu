<?php

require '../../vendor/autoload.php';
require '../../config/db_portal.php';
require '../../include/feedcreator.class.php';
 
$m = array(
	'xnxw' => '校内新闻',
	'xydt' => '学院动态',
	'tzgg' => '通知公告',
	'bmdt' => '部门动态',
	'jyjx' => '教育教学',
	'xsjl' => '学术交流',
	'xwgk' => '校务公开',
	'wjcx' => '文件查询');

$t = $m[$_GET['c']];

if (!$t) exit();
$t = $_GET['c'];

$cache_file = '/tmp/rss_'.md5($t).'.xml';
$data = $database->query("select * from (select NRID resource_id, title, BODY content, RELEASE_ORGANIZE dwmc, NAME tzlx, RELEASE_DATE, LMID from idc_u_biz.cms_content_yd where code='".$t."'  order by RELEASE_DATE desc) d where rownum < 5")->fetchAll();

$rss = new UniversalFeedCreator();
$rss->title = $t;
$rss->description = "西北工业大学，".$t;

foreach ($data as $d) {
     $date = DateTime::createFromFormat('Y-m-d H:i:s', $d[5]);

    $item = new FeedItem();
    $item->title = $d[1];
    $item->description = $d[2];
    $item->link = " https://ecampus.nwpu.edu.cn/web/guest/newscenter?p_l_id=0&p_p_id=newscenter_WAR_jigsawportalnewscenterportlet&p_p_lifecycle=0&_newscenter_WAR_jigsawportalnewscenterportlet_action=detail&_newscenter_WAR_jigsawportalnewscenterportlet_contentId=$d[0]&_newscenter_WAR_jigsawportalnewscenterportlet_channelId=$d[6]&&_newscenter_WAR_jigsawportalnewscenterportlet_releaseOrganize=$d[3]";
    $item->date = $date->getTimestamp();
    $rss->addItem($item);
}
$rss->saveFeed('1.0',$cache_file);
//$rss->saveFeed('1.0');
