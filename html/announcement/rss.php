<?php

require '../../vendor/autoload.php';
require '../../config/db_center.php';
require '../../include/feedcreator.class.php';
 
$m = array(
	'xnxw' => '校内新闻',
	'xydt' => '学院动态',
	'tzgg' => '通知公告',
	'bmdt' => '部门动态',
	'jyjx' => '教育教学',
	'xsjl' => '学术交流',
	'xwgk' => '校务公开',
	'wjcx' => '文件查询');

$t = $m[$_GET['c']];

if (!$t) exit();

$cache_file = '/tmp/rss_'.md5($t).'.xml';
$data = $database->query("select resource_id, title, content, dwmc, tzlx, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') create_time from idc_u_pub.view_tzgh where tzlx='".$t."' and rownum <= 5 order by create_time desc")->fetchAll();

$rss = new UniversalFeedCreator();
$rss->title = $t;
$rss->description = "西北工业大学，".$t;

foreach ($data as $d) {
     $date = DateTime::createFromFormat('Y-m-d H:i:s', $d[5]);

    $item = new FeedItem();
    $item->title = $d[1];
    $item->description = $d[2];
    $item->link = "http://portal.nwpu.edu.cn/dcp/forward.action?path=/portal/portal&p=pimHomePage#m=pim&t=pd&ptt=d&ptc=${d[0]}&pt=&pd=&ps=&psh=";
    $item->date = $date->getTimestamp();
    $rss->addItem($item);
}
$rss->saveFeed('1.0',$cache_file);
//$rss->saveFeed('1.0');
