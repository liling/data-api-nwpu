<?php

$database = new medoo([
    // required
    'database_type' => 'oracle',
    'database_name' => 'XE',
    'server' => '192.168.99.100',
    'username' => 'system',
    'password' => 'oracle',
    'charset' => 'utf8',
 
    // [optional]
    'port' => 1521,
 
    // [optional] Table prefix
    'prefix' => '',
]);
