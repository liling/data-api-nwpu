<?php

require '../vendor/autoload.php';

$database = new medoo([
    // required
    'database_type' => 'mysql',
    'database_name' => 'test',
    'server' => '127.0.0.1',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
 
    // [optional]
    'port' => 3306,
 
    // [optional] Table prefix
    'prefix' => '',
 
    // driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
    'option' => [
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);
